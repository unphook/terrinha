﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Tripod:MonoBehaviour {
		public Transform tr { get; private set; }
		public Camera cam { get; private set; }

		public float minVisibleAngle {
			get {
				return angle-visibleRange;
			}
		}
		public float maxVisibleAngle {
			get {
				return angle+visibleRange;
			}
		}
		public const float visibleRange = .8f;

		public float visibleSize = 7;

		public Gradient skyColour;
		public AudioClip[] song;
		public float[] songVolumeDay;
		public float[] songVolumeMid;
		public float[] songVolumeNight;
		float[] songVolume;

		Transform bg,clouds,stars;
		Renderer cloudsRend,starsRend;

		Color starsColour;

		float radius,angle,visible;
		float radiusNext,angleNext;
	
		const float followVel = 7;

		const float cloudsVisibilityFactor = 1.1f;
		const float starsVisibilityFactor = 1.25f;

		const float volumeFactor = .2f;

		AudioSource[] sources;
        bool playing;

		void Awake() {
			tr = transform;
			Game.mainCam = cam = GetComponent<Camera>();
			bg = tr.Find("bg");
			clouds = bg.Find("clouds");
			cloudsRend = clouds.GetComponent<Renderer>();
			stars = bg.Find("stars");
			starsRend = stars.GetComponent<Renderer>();

			starsColour = starsRend.material.GetColor("_TintColor");
			visible = visibleSize;

			sources = new AudioSource[song.Length];
            playing = false;
            songVolume = new float[song.Length];
			for (int a = 0; a < sources.Length; a++) {
				sources[a] = gameObject.AddComponent<AudioSource>();
				sources[a].clip = song[a];
				sources[a].loop = true;
				sources[a].volume = songVolume[a] = (a > 0) ? 0 : 1;
			}
		}
		
		public void FocusAt(Vector2 position,float radiusOffset = 0,bool snap = false) {
			radiusNext = position.magnitude+radiusOffset;
			angleNext = Mathf.Atan2(position.y,position.x);
			if (snap) {
				radius = radiusNext;
				angle = angleNext;
			} else {
				float angleMin = angleNext-Mathf.PI;
				float angleMax = angleNext+Mathf.PI;
				while (angle < angleMin) angle += Mathf.PI*2;
				while (angle > angleMax) angle -= Mathf.PI*2;
			}
		}

		void Update() {
			radius = Mathf.Lerp(radius,radiusNext,Time.deltaTime*followVel);
			angle = Mathf.Lerp(angle,angleNext,Time.deltaTime*followVel);
			visible = Mathf.Lerp(visible,visibleSize,Time.deltaTime*followVel*.5f);
			tr.localPosition = new Vector3(Mathf.Cos(angle)*radius,Mathf.Sin(angle)*radius,0);
			tr.localEulerAngles = new Vector3(0,0,angle*Mathf.Rad2Deg-90);

			float time = Level.me.time/Level.dayCycleDuration-angle/(Mathf.PI*2f);
			while (time >= 1) time--;
			while (time < 0) time++;
			float t = (time < .5f) ? (time*2) : (2-time*2);
			t = Tween.Ease(Tween.Ease(t));

			cam.backgroundColor = skyColour.Evaluate(t);

			float cloudsVisibility = Mathf.Clamp01(1-t*cloudsVisibilityFactor);
			cloudsRend.material.color = new Color(1,1,1,cloudsVisibility);

			float starsVisibility = Mathf.Clamp01((t-1)*starsVisibilityFactor+1);
			var newStarsColour = new Color(starsColour.r,starsColour.g,starsColour.b,starsVisibility*starsColour.a);
			starsRend.material.SetColor("_TintColor",newStarsColour);
			
			cam.orthographicSize = visible;
			bg.localScale = new Vector3(visible*4.2f,visible*4.2f,1);
			float scale = visible*.175f+.25f;
			float scaleOffset = -(scale-1)*.5f;
			var bgScale = new Vector2(scale,scale);
			var bgOffset = new Vector2(time*2+scaleOffset,radius*.03f+scaleOffset+.2f);
			cloudsRend.material.mainTextureScale = bgScale;
			starsRend.material.mainTextureScale = bgScale;
			cloudsRend.material.mainTextureOffset = bgOffset;
			starsRend.material.mainTextureOffset = bgOffset;

            if (!playing) {
                playing = true;
                for (int a = 0; a < sources.Length; a++) {
                    if (sources[a].clip.loadState != AudioDataLoadState.Loaded) {
                        playing = false;
                        break;
                    }
                }
                if (playing) {
                    for (int a = 0; a < sources.Length; a++) {
                        sources[a].Play();
                    }
                }
            }
            if (playing) {
                float[] volume0,volume1;
    			float volumeLerp;
    			if (t < .5f) {
    				volume0 = songVolumeDay;
    				volume1 = songVolumeMid;
    				volumeLerp = t*2;
    			} else {
    				volume0 = songVolumeMid;
    				volume1 = songVolumeNight;
    				volumeLerp = t*2-1;
    			}
    			float m = (Game.me.goToScene >= 0) ? Game.me.sceneTempo : 1;
    			for (int a = 0; a < sources.Length; a++) {
    				float v = songVolume[a];
    				float nv;
    				if (Level.me.hud.health <= 0) {
    					nv = (a > 0) ? 0 : 1;
    				} else {
    					nv = Mathf.Lerp(volume0[a],volume1[a],volumeLerp);
    				}
    				if (v > nv) {
    					v -= Time.deltaTime*volumeFactor;
    					if (v < nv) v = nv;
    				} else if (v < nv) {
    					v += Time.deltaTime*volumeFactor;
    					if (v > nv) v = nv;
    				}
    				songVolume[a] = v;
    				if (Game.me.goToScene >= 0) {
    					sources[a].volume = v*Game.me.sceneTempo;
    				} else {
    					sources[a].volume = v;
    				}
                }
			}
		}
	}
}