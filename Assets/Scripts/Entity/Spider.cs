﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Spider:EntityBase {
		public Entity entity { get; private set; }

		Transform sprite;
		Renderer rend;
		AudioSource aud;

		float roam = 0;
		float cooldown = 0;

		int lastDirection = 0;

		void Awake() {
			entity = GetComponent<Entity>();
			sprite = transform.Find("sprite");
			rend = sprite.GetComponent<Renderer>();
			aud = GetComponent<AudioSource>();
		}

		void Update() {
			float angle = entity.GetPlayerDistance();
			if (Mathf.Abs(angle) > Tripod.visibleRange*.8f) {
				roam -= Time.deltaTime;
				if (roam <= 0) {
					int direction;
					if (lastDirection == 0) {
						direction = Random.Range(-1,2);
					} else {
						direction = Random.Range(-1,1);
						if (direction >= lastDirection) direction++;
					}
					roam = (direction == 0) ? Random.Range(2f,5f) : Random.Range(1f,2f);
					entity.dir = lastDirection = direction;
				}
			} else {
				if (roam > 0) {
					roam = 0;
					entity.Jump();
				}
				entity.dir = -Mathf.Sign(entity.GetPlayerDistance());
			}
			if (Level.me.DetectPlayerClick(entity.tr.position,.5f)) {
				Level.me.ConsumePlayerClick();
				Level.me.ActivatePlayerCooldown();
				Hit(Level.me.GetPlayerAttackDamage()*.15f,Level.me.player.entity.angle);
			}
			float arrowAngle;
			if (Level.me.DetectArrow(entity.tr.position,.5f,out arrowAngle)) {
				Hit(Level.arrowAttackDamage*.15f,arrowAngle);
			}
			if (cooldown > 0) {
				cooldown -= Time.deltaTime;
				if (cooldown < 0) cooldown = 0;
			} else if (Mathf.Abs(entity.GetPlayerUnitDistance()) < .5f) {
				Level.me.player.Hit(1,entity.angle);
				cooldown = 1;
			}
			int pos = Level.GetFixedUnit(entity.angle);
			if (Level.me.fixedEntities[pos] != null && Level.me.fixedEntities[pos].type == FixedEntityType.Plantation) {
				Level.me.fixedEntities[pos].GetComponent<Plantation>().BadSpider();
			}
			if (Mathf.Approximately(entity.dir,0)) {
				rend.material.mainTextureOffset = Vector2.zero;
			} else {
				if (sprite.localScale.x*entity.dir < 0) {
					sprite.localScale = new Vector3(-sprite.localScale.x,sprite.localScale.y,sprite.localScale.z);
				}
				var i = ((int)(Time.time*8+lastDirection)%4)+1;
				if (i == 4) i = 2;
				rend.material.mainTextureOffset = new Vector2(0,i*.25f);
			}
		}
		
		public override bool Hit(float damage,float otherAngle) {
			aud.PlayOneShot(aud.clip);
			if (base.Hit(damage,otherAngle)) {
				Level.me.hud.GetItem(ItemType.Web,(int)Random.Range(.5f,2.9f));
				Level.me.Dust(entity.tr.position,25);
				GetComponent<Spawnable>().Destroy();
				return true;
			}
			entity.Repel(Mathf.Sign(entity.GetDistance(otherAngle))*2);
			entity.Jump();
			return false;
		}
	}
}