﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Spawnable:MonoBehaviour {
		public Level.SpawnProperties props;

		public void SetSpawn(Level.SpawnProperties props,Transform parent,float angle) {
			transform.parent = parent;
			this.props = props;
			GetComponent<Entity>().angle = angle;
			props.count++;
		}

		public void Destroy() {
			if (props != null && props.count > 0) props.count--;
			Destroy(gameObject);
		}
	}
}