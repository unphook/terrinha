﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class FixedEntity:EntityBase {
		public Entity entity { get; protected set; }
		public int fixedPosition { get; private set; }
		public FixedEntityType type { get; private set; }

		public void SetFixedAtAngle(float angle) {
			SetFixed(Level.GetFixedUnit(angle),angle);
		}

		public void SetFixedAtUnit(int pos) {
			SetFixed(pos,Level.GetAngleFromFixedUnit(pos));
		}

		public void SetFixed(int pos,float angle) {
			fixedPosition = pos;
			Level.me.fixedEntities[pos] = this;
			entity.angle = angle;
			type = CreateEntity();
		}

		public virtual void Destroy() {
			Destroy(gameObject);
			Level.me.fixedEntities[fixedPosition] = null;
			DestroyEntity();
		}

		protected virtual FixedEntityType CreateEntity() {
			return FixedEntityType.WhoCares;
		}
		protected virtual void DestroyEntity() { }
	}

	public enum FixedEntityType {
		WhoCares,
		Tree,
		Campfire,
		Plantation,
	}
}