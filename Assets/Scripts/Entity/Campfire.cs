﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Campfire:FixedEntity {
		Renderer rend;

		void Awake() {
			entity = GetComponent<Entity>();
			rend = transform.Find("sprite").GetComponent<Renderer>();
		}
		
		void Update() {
			rend.material.mainTextureOffset = new Vector2(((int)(Time.time*8)%4)*.25f,0);
			if (Level.me.DetectPlayerClick(entity.tr.position,1) && Level.me.hud.GetItem(ItemType.Campfire)) {
				Level.me.ConsumePlayerClick();
				Level.me.ConsumePlayerClick();
				Level.me.ActivatePlayerCooldown();
				Destroy();
			}
		}

		protected override FixedEntityType CreateEntity() {
			return FixedEntityType.Campfire;
		}

		protected override void DestroyEntity() {
			Level.me.Dust(entity.tr.position,15);
		}
	}
}