﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Entity:MonoBehaviour {
		public Transform tr { get; private set; }

		public float groundOffset = .5f;
		public float jumpForce = 3.5f;
		public float velocity = 2.5f;
		public float dirLerp = 16;

		float m_dir;
		public float dir {
			get {
				return m_dir;
			}
			set {
				m_dir = Mathf.Clamp(value,-1,1);
			}
		}

		float m_angle;
		public float angle {
			get {
				return m_angle;
			}
			set {
				m_angle = value;
				while (m_angle < -Mathf.PI) angle += Mathf.PI*2;
				while (m_angle > Mathf.PI) angle -= Mathf.PI*2;
				UpdatePosition();
			}
		}

		public bool grounded { get; private set; }
		public float height;
		public float upVelocity { get; private set; }

		bool jumpRequest = false;
		float currentDir = 0;

		void Awake() {
			tr = transform;
			m_angle = 0;
			grounded = true;
			height = 0;
			UpdatePosition();
		}

		void Start() {
			if (height > 0) grounded = false;
			UpdatePosition();
		}

		void Update() {
			if (currentDir < dir) {
				currentDir += Time.deltaTime*dirLerp;
				if (currentDir > dir) currentDir = dir;
			} else if (currentDir > dir) {
				currentDir -= Time.deltaTime*dirLerp;
				if (currentDir < dir) currentDir = dir;
			}
			bool updatePosition = false;
			if (jumpRequest) {
				jumpRequest = false;
				if (jumpForce > 0) upVelocity += jumpForce;
			}
			if (!Mathf.Approximately(upVelocity,0)) {
				height += upVelocity*Time.deltaTime;
				updatePosition = true;
				if (height <= 0) {
					height = 0;
					updatePosition = true;
					grounded = true;
					upVelocity = 0;
				} else {
					grounded = false;
				}
			}
			if (!grounded) upVelocity -= Level.gravity*Time.deltaTime;
			if (!Mathf.Approximately(currentDir,0)) {
				angle += Time.deltaTime*velocity*currentDir/(Level.worldRadius+height+groundOffset);
				updatePosition = true;
			}
			if (updatePosition) UpdatePosition();
		}

		void UpdatePosition() {
			float r = Level.worldRadius+height+groundOffset;
			tr.localPosition = new Vector3(-Mathf.Sin(angle)*r,Mathf.Cos(angle)*r,tr.localPosition.z);
		}
		
		public void Jump() {
			jumpRequest = true;
		}

		public void Repel(float amount) {
			currentDir = amount;
		}

		public float GetDistance(float otherAngle) {
			float a = angle-otherAngle;
			while (a < -Mathf.PI) a += Mathf.PI*2;
			while (a > Mathf.PI) a -= Mathf.PI*2;
			return a;
		}

		public float GetUnitDistance(float otherAngle) {
			return GetDistance(otherAngle)*Level.worldRadius;
		}

		public float GetPlayerDistance() {
			return GetDistance(Level.me.player.entity.angle);
		}

		public float GetPlayerUnitDistance() {
			return GetPlayerDistance()*Level.worldRadius;
		}
	}
}