﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Player:EntityBase {
		public Entity entity { get; private set; }

		public AudioClip sfxWalk,sfxHit,sfxSwing,sfxConsume,sfxAction;

		Transform sprite;
		Renderer rend;
		AudioSource aud;

		float keepTexture = 0;

		void Awake() {
			entity = GetComponent<Entity>();
			sprite = transform.Find("sprite");
			rend = sprite.GetComponent<Renderer>();
			aud = GetComponent<AudioSource>();
		}

		void Update() {
			if (Level.me.hud.health <= 0 || Level.me.hud.homeMenuOpen != HudHomeMenu.None) {
				entity.dir = 0;
				if (Level.me.hud.health <= 0) {
					rend.material.mainTextureOffset = new Vector2(.75f,.5f);
				}
				return;
			}
			bool jump;
			if (Input.Hold(KeyCode.A)) {
				if (Input.Hold(KeyCode.D)) {
					entity.dir = 0;
					jump = false;
				} else {
					entity.dir = 1;
					jump = true;
				}
			} else if (Input.Hold(KeyCode.D)) {
				entity.dir = -1;
				jump = true;
			} else {
				entity.dir = 0;
				jump = false;
			}
			if (entity.grounded && jump) {
				entity.Jump();
				aud.PlayOneShot(sfxWalk,.5f);
			}
			var i = Level.me.hud.selectedItem;
			if (i != null) {
				var p = entity.tr.position;
				const float r = Level.playerActionRadius;
				switch (i.type) {
					case ItemType.Carrot:
					case ItemType.RawBeef:
					case ItemType.RoastBeef:
					case ItemType.FilledBucket:
						if (Level.me.DetectPlayerClick(p,.5f)) {
							Level.me.ConsumePlayerClick();
							Level.me.ActivatePlayerCooldown();
							Level.me.hud.Consume();
							aud.PlayOneShot(sfxConsume);
						}
						break;
					case ItemType.Campfire:
					case ItemType.Sapling:
					case ItemType.Hoe:
						if (Level.me.DetectPlayerClick(p,r) && Level.me.PlaceEntity()) {
							Level.me.ConsumePlayerClick();
						}
						break;
					case ItemType.Bow:
						if (Level.me.mouseFree && Input.mousePress && Level.me.hud.Consume(ItemType.Arrow,1)) {
							Level.me.FireArrow(
								entity.groundOffset+entity.height,
								entity.angle,
								Level.me.mouseRadius-Level.worldRadius,
								Level.me.mouseAngle
							);
							aud.PlayOneShot(sfxSwing);
						}
						break;
				}
			}
			if (keepTexture > 0) {
				keepTexture -= Time.deltaTime;
				if (keepTexture < 0) keepTexture = 0;
			} else if (entity.grounded && Mathf.Approximately(entity.dir,0)) {
				rend.material.mainTextureOffset = Vector2.zero;
			} else {
				if (sprite.localScale.x*entity.dir < 0) {
					sprite.localScale = new Vector3(-sprite.localScale.x,sprite.localScale.y,sprite.localScale.z);
				}
				float vel = entity.upVelocity/entity.jumpForce;
				int frame;
				if (Mathf.Abs(vel) <= 1/3f) {
					frame = 2;
				} else {
					frame = (vel > 0) ? 1 : 3;
				}
				rend.material.mainTextureOffset = new Vector2(frame*.25f,0);
			}
		}

		public void PlaySfx(AudioClip clip) {
			aud.PlayOneShot(clip);
		}

		public void ItemAction() {
			var i = Level.me.hud.selectedItem;
			if (i == null) return;
			switch (i.type) {
				case ItemType.Sword:
					rend.material.mainTextureOffset = new Vector2(0,.5f);
					aud.PlayOneShot(sfxSwing);
					break;
				case ItemType.Axe:
					rend.material.mainTextureOffset = new Vector2(.25f,.5f);
					aud.PlayOneShot(sfxSwing);
					break;
				case ItemType.Hoe:
					rend.material.mainTextureOffset = new Vector2(.5f,.5f);
					aud.PlayOneShot(sfxSwing);
					break;
				default: return;
			}
			keepTexture = .25f;
		}

		public override bool Hit(float damage,float otherAngle) {
			aud.PlayOneShot(sfxHit);
			if (!Mathf.Approximately(otherAngle,0)) {
				entity.Repel(Mathf.Sign(entity.GetDistance(otherAngle))*2);
			}
			entity.Jump();
			Level.me.hud.PlayerHit(Mathf.RoundToInt(damage));
			return false;
		}
	}
}