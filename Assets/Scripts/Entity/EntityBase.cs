﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class EntityBase:MonoBehaviour {
		public float health = 1;

		public virtual bool Hit(float damage,float otherAngle) {
			health -= damage;
			if (health <= 0) {
				health = 0;
				return true;
			}
			return false;
		}
	}
}