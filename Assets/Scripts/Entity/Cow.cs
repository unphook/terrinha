﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Cow:EntityBase {
		public Entity entity { get; private set; }

		Transform sprite;
		Renderer rend;
		AudioSource aud;

		public float defaultVelocity = 1;
		public float runVelocity = 2.5f;
		
		float roam = 0;
		float run = 0;

		void Awake() {
			entity = GetComponent<Entity>();
			entity.velocity = defaultVelocity;
			sprite = transform.Find("sprite");
			rend = sprite.GetComponent<Renderer>();
			aud = GetComponent<AudioSource>();
		}

		void Update() {
			UpdateDirection();
			if (Level.me.DetectPlayerClick(entity.tr.position,1)) {
				Level.me.ConsumePlayerClick();
				Level.me.ActivatePlayerCooldown();
				Hit(Level.me.GetPlayerAttackDamage()*.11f,Level.me.player.entity.angle);
			}
			float arrowAngle;
			if (Level.me.DetectArrow(entity.tr.position,1,out arrowAngle)) {
				Hit(Level.arrowAttackDamage*.11f,arrowAngle);
			}
			if (Mathf.Approximately(entity.dir,0)) {
				rend.material.mainTextureOffset = Vector2.zero;
			} else {
				if (sprite.localScale.x*entity.dir < 0) {
					sprite.localScale = new Vector3(-sprite.localScale.x,sprite.localScale.y,sprite.localScale.z);
				}
				var i = ((int)(Time.time*entity.velocity*8)%4)+1;
				if (i == 4) i = 2;
				rend.material.mainTextureOffset = new Vector2(0,i*.25f);
			}
		}

		void UpdateDirection() {
			if (run > 0) {
				run -= Time.deltaTime;
				if (run <= 0) {
					run = 0;
					roam = 0;
					entity.velocity = defaultVelocity;
				} else {
					return;
				}
			}
			roam -= Time.deltaTime;
			if (roam <= 0) {
				roam = Random.Range(2f,5f);
				entity.dir = Random.Range(-1,2);
			}
		}

		public override bool Hit(float damage,float otherAngle) {
			aud.PlayOneShot(aud.clip);
			if (base.Hit(damage,otherAngle)) {
				Level.me.hud.GetItem(ItemType.RawBeef,(int)Random.Range(.5f,2.9f));
				Level.me.Dust(entity.tr.position,30);
				GetComponent<Spawnable>().Destroy();
				return true;
			}
			run = Random.Range(8f,12f);
			entity.Jump();
			var newDir = Mathf.Sign(entity.GetDistance(otherAngle));
			entity.Repel(newDir*2);
			entity.dir = newDir;
			entity.velocity = runVelocity;
			return false;
		}
	}
}