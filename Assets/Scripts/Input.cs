﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public static class Input {
		public static float mouseWheel {
			get {
				return UnityEngine.Input.mouseScrollDelta.y;
			}
		}

		public static bool mousePress {
			get {
				return UnityEngine.Input.GetMouseButtonDown(0);
			}
		}
		public static bool mouseHold {
			get {
				return UnityEngine.Input.GetMouseButton(0);
			}
		}
		public static bool mouseRelease {
			get {
				return UnityEngine.Input.GetMouseButtonUp(0);
			}
		}
		public static bool mousePressAlt {
			get {
				return UnityEngine.Input.GetMouseButtonDown(1);
			}
		}
		public static bool mouseHoldAlt {
			get {
				return UnityEngine.Input.GetMouseButton(1);
			}
		}
		public static bool mouseReleaseAlt {
			get {
				return UnityEngine.Input.GetMouseButtonUp(1);
			}
		}

		public static bool Press(KeyCode key) {
			return UnityEngine.Input.GetKeyDown(key);
		}
		public static bool Hold(KeyCode key) {
			return UnityEngine.Input.GetKey(key);
		}
		public static bool Release(KeyCode key) {
			return UnityEngine.Input.GetKeyUp(key);
		}

		public static bool GetMouseAt(Camera camera,out Vector2 pos) {
			var spos = UnityEngine.Input.mousePosition;
			pos = camera.ScreenToWorldPoint(spos);
			return spos.x >= Window.fullRect.xMin*Window.width
				&& spos.x <= Window.fullRect.xMax*Window.width
				&& spos.y >= Window.fullRect.yMin*Window.height
				&& spos.y <= Window.fullRect.yMax*Window.height;
		}
	}
}