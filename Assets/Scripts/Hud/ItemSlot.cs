﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class ItemSlot:MonoBehaviour {
		public bool select = false;

		float tempo = 0;

		public Transform tr { get; private set; }
		Renderer rend;

		void Awake() {
			tr = transform;
			rend = GetComponent<Renderer>();
			UpdateColour();
		}

		void Update() {
			const float vel = 12;
			if (select) {
				if (tempo < 1) {
					tempo = 1;
					UpdateColour();
				}
			} else {
				if (tempo > 0) {
					tempo -= Time.deltaTime*vel;
					if (tempo < 0) tempo = 0;
					UpdateColour();
				}
			}
		}
		
		void UpdateColour() {
			float c = Tween.Ease(tempo)*.75f;
			rend.material.color = new Color(c,c,c,1);
		}

		public bool PosInside(Vector2 pos) {
			var p = tr.position;
			const float r = .5f;
			return pos.x >= p.x-r && pos.x <= p.x+r && pos.y >= p.y-r && pos.y <= p.y+r;
		}
	}
}