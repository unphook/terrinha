﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class CursorObj:MonoBehaviour {
		Transform tr;
		Renderer rend;
		ItemObj item;

		float size,rot,alpha;

		const float sizeDefault = .5f;
		const float sizeHold = .45f;
		const float sizeRelease = .55f;
		const float rotAngle = 10;

		void Awake() {
			tr = transform;
			rend = tr.Find("sprite").GetComponent<Renderer>();
			item = tr.Find("item").GetComponent<ItemObj>();

			size = rot = alpha = 0;

			tr.localScale = new Vector3(sizeDefault,sizeDefault,1);
			tr.localEulerAngles = Vector3.zero;
			rend.enabled = false;
		}

		void Start() {
			item.quantity = 0;
			item.rend.enabled = false;
			item.rend.material.color = Color.clear;
		}

		void Update() {
			if (!Level.me.mouseFree || Level.me.hud.selectedItem == null) {
				item.quantity = 0;
				item.rend.enabled = false;
			} else {
				item.quantity = 1;
				item.type = Level.me.hud.selectedItem.type;
				item.rend.enabled = true;
			}
			if (Level.me.mousePresent) {
				tr.localPosition = new Vector3(Level.me.mouseHud.x,Level.me.mouseHud.y,tr.localPosition.z);
				if (alpha < 1) {
					alpha = 1;
					item.rend.enabled = rend.enabled = true;
					item.rend.material.color = rend.material.color = Color.white;
				}
			} else if (alpha > 0) {
				alpha -= Time.deltaTime*2;
				if (alpha <= 0) {
					alpha = 0;
					rend.enabled = false;
				} else {
					rend.material.color = new Color(1,1,1,Tween.EaseOut(alpha));
				}
				item.rend.material.color = new Color(1,1,1,Tween.EaseOut(alpha));
			}
			if (Level.me.mousePresent && Input.mouseHold) {
				tr.localScale = new Vector3(sizeHold,sizeHold,1);
			} else if (Level.me.mousePresent && Input.mouseRelease) {
				size = 1;
				tr.localScale = new Vector3(sizeRelease,sizeRelease,1);
			} else if (size > 0) {
				size -= Time.deltaTime*8;
				if (size < 0) size = 0;
				float s = Mathf.Lerp(sizeDefault,sizeRelease,Tween.EaseIn(size));
				tr.localScale = new Vector3(s,s,1);
			}
			bool animRot = false;
			if (Level.me.mousePresent && !Mathf.Approximately(0,Input.mouseWheel)) {
				rot = Mathf.Sign(Input.mouseWheel);
				animRot = true;
			} else if (rot > 0) {
				rot -= Time.deltaTime*8;
				if (rot < 0) rot = 0;
				animRot = true;
			} else if (rot < 0) {
				rot += Time.deltaTime*8;
				if (rot > 0) rot = 0;
				animRot = true;
			}
			if (animRot) {
				tr.localEulerAngles = new Vector3(0,0,Tween.EaseIn(Mathf.Abs(rot))*Mathf.Sign(rot)*rotAngle);
			}
		}
	}
}