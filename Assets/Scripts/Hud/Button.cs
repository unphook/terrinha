﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Button:MonoBehaviour {
		public Transform tr { get; private set; }

		public bool hover = false;
		bool prevHover = false;
		
		bool click = false;
		public bool release { get; private set; }

		Vector3 size;
		float w,h;

		float tempo = 0;

		void Awake() {
			tr = transform;
		}

		void Start() {
			size = tr.localScale;
			w = tr.lossyScale.x*.5f;
			h = tr.lossyScale.y*.5f;
			UpdateScale();
		}

		void Update() {
			release = false;
			if (hover) {
				if (!prevHover) {
					prevHover = true;
					Level.me.hud.PlaySfx(false);
				}
				if (Input.mousePress) click = true;
				if (click && !Input.mouseHold) {
					click = false;
					release = true;
					Level.me.hud.PlaySfx(true);
				}
			} else {
				prevHover = false;
				if (click && !Input.mouseHold) click = false;
			}
			float t = hover ? (click ? -1 : 1) : 0;
			const float vel = 20;
			if (tempo < t) {
				tempo += Time.deltaTime*vel;
				if (tempo > t) tempo = t;
				UpdateScale();
			} else if (tempo > t) {
				tempo -= Time.deltaTime*vel;
				if (tempo < t) tempo = t;
				UpdateScale();
			}
		}
		
		public bool PosInside(Vector2 pos) {
			Vector2 p = tr.position;
			return pos.x >= p.x-w && pos.x <= p.x+w && pos.y >= p.y-h && pos.y <= p.y+h;
		}

		void UpdateScale() {
			float m = 1+tempo*.05f;
			tr.localScale = new Vector3(size.x*m,size.y*m,size.z);
		}
	}
}