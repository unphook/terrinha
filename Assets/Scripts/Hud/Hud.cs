﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Hud:MonoBehaviour {
		public Transform tr { get; private set; }
		public Camera cam { get; private set; }

		public AudioClip sfxHover,sfxClick,sfxMenuOpen,sfxMenuClose;

		AudioSource aud;

		public int currentItem { get; private set; }
		public ItemSlot[] inventorySlots { get; private set; }
		public ItemObj[] inventory { get; private set; }
		public ItemSlot inventoryTrash { get; private set; }
		public ItemObj selectedItem {
			get {
				return inventory[currentItem];
			}
			set {
				inventory[currentItem] = value;
			}
		}

		public const int inventoryCount = 9;

		ItemObj itemDrag = null;
		bool itemDragHalf = false;

		public float hungry { get; private set; }
		public float thirsty { get; private set; }
		public int health { get; private set; }

		float healthTempo = 0;
		LifeMeter hungryMeter,thirstyMeter;

		public const float hungryDuration = 9*60;
		public const float thirstyDuration = 6*60;
		public const float healthDuration = 5;
		public const float healthRecovery = .65f/1.05f;
		public const int maxHealth = 6;
		
		public HudHomeMenu homeMenuOpen { get; private set; }

		Transform homeHud;
		Button homeCraftingButton,homeStorageButton;
		Transform homeMenu;
		Button homeMenuCloseButton;

		Transform craftingMenu;
		Transform[,] craftingRows;
		ItemObj[,] craftingItems;
		Button craftingUpButton,craftingDownButton;
		int craftingRow = 0;
		Dictionary<ItemType,int> recipeDict;

		public const int craftingRowCount = 4;
		public const int craftingColumnCount = 4;
		public static List<ItemRecipe> itemRecipes = null;

		Transform storageMenu;
		ItemSlot[] storageSlots;
		ItemObj[] storage;

		public const int storageCount = 20;

		float homeShowTempo = 0;
		float homeMenuShowTempo = 0;

		Renderer deadMessage,deadMessage2;
		Button deadButton;

		void Awake() {
			tr = transform;
			cam = tr.Find("cam").GetComponent<Camera>();

			aud = GetComponent<AudioSource>();

			hungry = .8f;
			thirsty = .8f;
			health = maxHealth;
			
			hungryMeter = tr.Find("hungry").GetComponent<LifeMeter>();
			thirstyMeter = tr.Find("thirsty").GetComponent<LifeMeter>();

			currentItem = 0;
			inventorySlots = new ItemSlot[inventoryCount];
			inventory = new ItemObj[inventoryCount];
			var inventoryParent = tr.Find("inventory");
			inventoryTrash = inventoryParent.Find("trash").GetComponent<ItemSlot>();
			inventoryTrash.select = false;
			for (int a = 0; a < inventoryCount; a++) {
				inventorySlots[a] = inventoryParent.Find(a.ToString()).GetComponent<ItemSlot>();
				inventorySlots[a].select = a == currentItem;
			}

			homeMenuOpen = HudHomeMenu.None;
			
			homeHud = tr.Find("home");
			homeCraftingButton = homeHud.Find("crafting").GetComponent<Button>();
			homeStorageButton = homeHud.Find("storage").GetComponent<Button>();
			homeMenu = tr.Find("homemenu");
			homeMenuCloseButton = homeMenu.Find("close").GetComponent<Button>();

			craftingMenu = homeMenu.Find("crafting");
			craftingRows = new Transform[craftingRowCount,craftingColumnCount];
			craftingItems = new ItemObj[craftingRowCount,craftingColumnCount];
			craftingUpButton = craftingMenu.Find("up").GetComponent<Button>();
			craftingDownButton = craftingMenu.Find("down").GetComponent<Button>();
			recipeDict = new Dictionary<ItemType,int>();
			for (int a = 0; a < craftingRowCount; a++) {
				var row = craftingMenu.Find(a.ToString());
				for (int b = 0; b < craftingRowCount; b++) {
					craftingRows[a,b] = row.Find(b.ToString());
					craftingItems[a,b] = ItemObj.Create(craftingMenu);
					craftingItems[a,b].origin = ItemOrigin.Crafting;
					craftingItems[a,b].position = a;
					var p = craftingRows[a,b].position;
					craftingItems[a,b].tr.position = new Vector3(p.x,p.y,-1);
				}
			}

			storageMenu = homeMenu.Find("storage");
			storageSlots = new ItemSlot[storageCount];
			storage = new ItemObj[storageCount];
			for (int a = 0; a < storageCount; a++) {
				storageSlots[a] = storageMenu.Find(a.ToString()).GetComponent<ItemSlot>();
				storageSlots[a].select = false;
				storage[a] = null;
			}

			deadMessage = tr.Find("deadmsg").GetComponent<Renderer>();
			var deadButtonTr = tr.Find("deadbutton");
			deadButton = deadButtonTr.GetComponent<Button>();
			deadMessage2 = deadButtonTr.Find("deadmsg").GetComponent<Renderer>();
			deadMessage.enabled = deadMessage2.enabled = false;
		}

		void Start() {
			for (int a = 0; a < inventoryCount; a++) {
				if (inventory[a] != null) UpdateItemPosition(inventory[a]);
			}
			homeHud.localScale = Vector3.zero;
			homeMenu.localScale = Vector3.zero;
			SetCraftingRecipes();
			GetItem(ItemType.Sword);
			GetItem(ItemType.Axe);
			GetItem(ItemType.Hoe);
			GetItem(ItemType.CarrotSeed,8);
		}

		void Update() {
			UpdateMeters();
			UpdateHomeHud();
			UpdateInventorySelection();

			if (Level.me.mousePresent) {
				if (health > 0 && Input.mousePress) {
					if (itemDrag != null) {
						UpdateItemPosition(itemDrag);
						itemDrag = null;
					}
					for (int a = 0; a < inventoryCount; a++) {
						if (inventory[a] != null && inventory[a].PosInside(Level.me.mouseHud)) {
							itemDrag = inventory[a];
							break;
						}
					}
					if (itemDrag == null) {
						if (homeMenuOpen == HudHomeMenu.Crafting) {
							for (int a = 0; a < craftingRowCount; a++) {
								if (craftingItems[a,0].PosInside(Level.me.mouseHud) && CraftRecipeCheck(a+craftingRow)) {
									itemDrag = craftingItems[a,0];
								}
							}
						} else if (homeMenuOpen == HudHomeMenu.Storage) {
							for (int a = 0; a < storageCount; a++) {
								if (storage[a] != null && storage[a].PosInside(Level.me.mouseHud)) {
									itemDrag = storage[a];
									break;
								}
							}
						}
					}
					if (itemDrag != null && itemDrag.origin != ItemOrigin.Crafting && itemDrag.quantity > 1 && Input.Hold(KeyCode.LeftShift)) {
						itemDragHalf = true;
						int nq = itemDrag.quantity-itemDrag.quantity/2;
						itemDrag.quantity -= nq;
						var i = itemDrag;
						itemDrag = ItemObj.Create(i.type,nq);
						itemDrag.origin = i.origin;
						itemDrag.position = i.position;
					} else {
						itemDragHalf = false;
					}
					if (itemDrag != null) {
						PlaySfx(true);
					}
				}
				
				if (health <= 0 || Input.mouseRelease) {
					if (itemDrag != null) {
						bool release = false;
						bool merge = false;
						int p = itemDrag.position;
						var o = itemDrag.origin;
						int maxStack = ItemObj.MaxStack(itemDrag.type);
						for (int a = 0; a < inventoryCount; a++) {
							if (o == ItemOrigin.Inventory && p == a) continue;
							if (inventorySlots[a].PosInside(Level.me.mouseHud)) {
								if (inventory[a] != null) {
									if (inventory[a].type == itemDrag.type) {
										int c = inventory[a].quantity+itemDrag.quantity;
										if (c <= maxStack) {
											inventory[a].quantity = c;
											if (o == ItemOrigin.Crafting) {
												UpdateItemPosition(itemDrag);
												CraftRecipeCheck(itemDrag.position+craftingRow,true);
												itemDrag = null;
											}
											merge = true;
										} else if (o != ItemOrigin.Crafting) {
											inventory[a].quantity = maxStack;
											itemDrag.quantity = c-maxStack;
										}
										break;
									} else {
										if (o == ItemOrigin.Crafting || itemDragHalf) break;
									}
									inventory[a].position = p;
									inventory[a].origin = o;
									UpdateItemPosition(inventory[a]);
								}
								if (!itemDragHalf) {
									switch (o) {
										case ItemOrigin.Inventory:
											inventory[p] = inventory[a];
											break;
										case ItemOrigin.Crafting:
											UpdateItemPosition(itemDrag);
											itemDrag = CraftRecipe(itemDrag.position+craftingRow);
											break;
										case ItemOrigin.Storage:
											storage[p] = inventory[a];
											break;
									}
								}
								inventory[a] = itemDrag;
								itemDrag.origin = ItemOrigin.Inventory;
								itemDrag.position = a;
								release = true;
								break;
							}
						}
						if (!release && !merge && o != ItemOrigin.Crafting) {
							if (inventoryTrash.PosInside(Level.me.mouseHud)) {
								if (!itemDragHalf) {
									switch (o) {
										case ItemOrigin.Inventory:
											inventory[p] = null;
											break;
										case ItemOrigin.Storage:
											storage[p] = null;
											break;
									}
								}
								merge = true;
							}
						}
						if (!release && !merge && homeMenuOpen == HudHomeMenu.Storage) {
							for (int a = 0; a < storageCount; a++) {
								if (o == ItemOrigin.Storage && p == a) continue;
								if (storageSlots[a].PosInside(Level.me.mouseHud)) {
									if (storage[a] != null) {
										if (storage[a].type == itemDrag.type) {
											int c = storage[a].quantity+itemDrag.quantity;
											if (c <= maxStack) {
												storage[a].quantity = c;
												if (o == ItemOrigin.Crafting) {
													UpdateItemPosition(itemDrag);
													CraftRecipeCheck(itemDrag.position+craftingRow,true);
													itemDrag = null;
												}
												merge = true;
											} else if (o != ItemOrigin.Crafting) {
												storage[a].quantity = maxStack;
												itemDrag.quantity = c-maxStack;
											}
											break;
										} else {
											if (o == ItemOrigin.Crafting || itemDragHalf) break;
										}
										storage[a].position = p;
										storage[a].origin = o;
										UpdateItemPosition(storage[a]);
									}
									if (!itemDragHalf) {
										switch (o) {
											case ItemOrigin.Inventory:
												inventory[p] = storage[a];
												break;
											case ItemOrigin.Crafting:
												UpdateItemPosition(itemDrag);
												itemDrag = CraftRecipe(itemDrag.position+craftingRow);
												break;
											case ItemOrigin.Storage:
												storage[p] = storage[a];
												break;
										}
									}
									storage[a] = itemDrag;
									itemDrag.origin = ItemOrigin.Storage;
									itemDrag.position = a;
									release = true;
									break;
								}
							}
						}
						if (merge) {
							if (itemDrag != null) Destroy(itemDrag.gameObject);
						} else if (!release && itemDragHalf) {
							switch (o) {
								case ItemOrigin.Inventory:
									inventory[p].quantity += itemDrag.quantity;
									break;
								case ItemOrigin.Storage:
									storage[p].quantity += itemDrag.quantity;
									break;
							}
							Destroy(itemDrag.gameObject);
						} else {
							UpdateItemPosition(itemDrag);
						}
						if (merge || release) {
							PlaySfx(true);
						}
						itemDrag = null;
					}
				}

				if (itemDrag != null) {
					UpdateItemPosition(itemDrag,true);
				}
			}

			if (health <= 0) {
				if (homeMenuOpen != HudHomeMenu.None) {
					SetHomeMenu(HudHomeMenu.None);
				}
				deadButton.hover = Level.me.mousePresent && deadButton.PosInside(Level.me.mouseHud);
				if (deadButton.release) {
					Game.ChangeScene(0);
				}
			}
		}

		void UpdateMeters() {
			if (health > 0) {
				if (hungry > 0) {
					hungry -= Time.deltaTime/hungryDuration;
					if (hungry < 0) hungry = 0;
				}
				if (thirsty > 0) {
					thirsty -= Time.deltaTime/thirstyDuration;
					if (thirsty < 0) thirsty = 0;
				}
				if (hungry >= healthRecovery && thirsty >= healthRecovery) {
					if (health < maxHealth) {
						healthTempo -= Time.deltaTime;
						if (healthTempo <= 0) {
							healthTempo = healthDuration;
							health++;
						}
					} else {
						healthTempo = healthDuration;
					}
				} else if (hungry <= 0 || thirsty <= 0) {
					healthTempo -= Time.deltaTime;
					if (healthTempo <= 0) {
						healthTempo = healthDuration;
						Level.me.player.Hit(1,0);
					}
				} else {
					healthTempo = healthDuration;
				}
				hungryMeter.value = hungry*1.05f;
				thirstyMeter.value = thirsty*1.05f;
			}
		}

		void UpdateHomeHud() {
			const float homeProximity = .125f;
			bool atHome = Level.me.player.entity.angle > -homeProximity && Level.me.player.entity.angle < homeProximity;
			var homePos = Level.me.homeTr.position;
			var homeGui = WorldToHud(new Vector2(homePos.x,homePos.y+2));
			homeHud.position = Vector2.Lerp(homeHud.position,homeGui,Time.deltaTime*8);

			bool pr = Level.me.mousePresent && health > 0;

			homeCraftingButton.hover = pr && atHome && homeMenuOpen == HudHomeMenu.None && homeCraftingButton.PosInside(Level.me.mouseHud);
			homeStorageButton.hover = pr && atHome && homeMenuOpen == HudHomeMenu.None && homeStorageButton.PosInside(Level.me.mouseHud);
			homeMenuCloseButton.hover = pr && homeMenuOpen != HudHomeMenu.None && homeMenuCloseButton.PosInside(Level.me.mouseHud);
			craftingUpButton.hover = pr && homeMenuOpen == HudHomeMenu.Crafting && craftingUpButton.PosInside(Level.me.mouseHud);
			craftingDownButton.hover = pr && homeMenuOpen == HudHomeMenu.Crafting && craftingDownButton.PosInside(Level.me.mouseHud);
			
			if (homeCraftingButton.release) {
				SetHomeMenu(HudHomeMenu.Crafting);
			} else if (homeStorageButton.release) {
				SetHomeMenu(HudHomeMenu.Storage);
			} else if (homeMenuCloseButton.release) {
				SetHomeMenu(HudHomeMenu.None);
			} else if (craftingUpButton.release) {
				if (craftingRow > 0) {
					craftingRow--;
					SetCraftingRecipes();
				}
			} else if (craftingDownButton.release) {
				if (craftingRow < itemRecipes.Count-craftingRowCount) {
					craftingRow++;
					SetCraftingRecipes();
				}
			}

			if (health > 0 && atHome && homeMenuOpen == HudHomeMenu.None) {
				if (homeShowTempo < 1) {
					homeShowTempo += Time.deltaTime*4;
					if (homeShowTempo > 1) homeShowTempo = 1;
					homeHud.localScale = Vector3.one*Tween.EaseOut(homeShowTempo);
				}
			} else {
				if (homeShowTempo > 0) {
					homeShowTempo -= Time.deltaTime*4;
					if (homeShowTempo < 0) homeShowTempo = 0;
					homeHud.localScale = Vector3.one*Tween.EaseOut(homeShowTempo);
				}
			}

			if (health > 0 && homeMenuOpen != HudHomeMenu.None) {
				if (homeMenuShowTempo < 1) {
					homeMenuShowTempo += Time.deltaTime*4;
					if (homeMenuShowTempo > 1) homeMenuShowTempo = 1;
					homeMenu.localScale = Vector3.one*Tween.EaseOut(homeMenuShowTempo);
				}
			} else {
				if (homeMenuShowTempo > 0) {
					homeMenuShowTempo -= Time.deltaTime*4;
					if (homeMenuShowTempo < 0) homeMenuShowTempo = 0;
					homeMenu.localScale = Vector3.one*Tween.EaseOut(homeMenuShowTempo);
				}
			}
		}

		void SetHomeMenu(HudHomeMenu menu) {
			if (homeMenuOpen == menu) return;
			homeMenuOpen = menu;
			var vanish = new Vector3(0,0,500);
			if (menu == HudHomeMenu.Crafting) {
				craftingMenu.localPosition = Vector3.zero;
				storageMenu.localPosition = vanish;
				aud.PlayOneShot(sfxMenuOpen);
			} else if (menu == HudHomeMenu.Storage) {
				craftingMenu.localPosition = vanish;
				storageMenu.localPosition = Vector3.zero;
				aud.PlayOneShot(sfxMenuOpen);
			} else {
				aud.PlayOneShot(sfxMenuClose);
			}
		}

		void SetCraftingRecipes() {
			if (itemRecipes == null) {
				itemRecipes = new List<ItemRecipe> {
					new ItemRecipe(
						ItemType.RoastBeef,1,
						ItemType.RawBeef,1,
						ItemType.Wood,1
					),
					new ItemRecipe(
						ItemType.Bucket,1,
						ItemType.Wood,8
					),
					new ItemRecipe(
						ItemType.Bow,1,
						ItemType.Wood,4,
						ItemType.Web,4
					),
					new ItemRecipe(
						ItemType.Arrow,3,
						ItemType.Wood,2
					),
					new ItemRecipe(
						ItemType.Campfire,1,
						ItemType.Wood,10
					),
					new ItemRecipe(
						ItemType.CarrotSeed,2,
						ItemType.Carrot,1
					),
				};
			}
			for (int a = 0; a < craftingRowCount; a++) {
				var r = itemRecipes[a+craftingRow];
				for (int b = 0; b < craftingColumnCount; b++) {
					var i = craftingItems[a,b];
					if (r.quantity[b] > 0) {
						i.quantity = r.quantity[b];
						i.type = r.type[b];
					} else {
						i.quantity = 0;
					}
				}
			}
		}

		void CreateRecipeDict(int index) {
			recipeDict = new Dictionary<ItemType,int>();
			var r = itemRecipes[index];
			for (int a = 1; a < r.type.Length; a++) {
				if (r.quantity[a] <= 0) continue;
				int q;
				if (!recipeDict.TryGetValue(r.type[a],out q)) q = 0;
				recipeDict[r.type[a]] = q+r.quantity[a];
			}
		}

		bool CraftRecipeCheck(int index,bool consumeItems = false) {
			CreateRecipeDict(index);
			for (int a = 0; a < inventoryCount; a++) {
				var i = inventory[a];
				int q;
				if (i != null && recipeDict.TryGetValue(i.type,out q)) {
					var iq = i.quantity;
					if (consumeItems) {
						if (iq <= q) {
							Destroy(i.gameObject);
							inventory[a] = null;
						} else {
							i.quantity -= q;
						}
					}
					q -= iq;
					if (q <= 0) {
						if (recipeDict.Count <= 1) {
							recipeDict.Clear();
							return true;
						}
						recipeDict.Remove(i.type);
					} else {
						recipeDict[i.type] = q;
					}
				}
			}
			recipeDict.Clear();
			return false;
		}

		ItemObj CraftRecipe(int index) {
			CraftRecipeCheck(index,true);
			var r = itemRecipes[index];
			return ItemObj.Create(r.type[0],r.quantity[0]);
		}

		void UpdateInventorySelection() {
			if (health <= 0) return;
			int prevItem = currentItem;
			if (Input.mouseWheel > 0) {
				currentItem--;
				while (currentItem < 0) currentItem += inventoryCount;
			} else if (Input.mouseWheel < 0) {
				currentItem++;
				while (currentItem >= inventoryCount) currentItem -= inventoryCount;
			} else {
				for (int a = 0; a < 9; a++) {
					if (Input.Press(KeyCode.Alpha1+a)) {
						currentItem = a;
						break;
					}
				}
			}
			if (prevItem != currentItem) {
				inventorySlots[prevItem].select = false;
				inventorySlots[currentItem].select = true;
				PlaySfx(false);
			}
		}

		void UpdateItemPosition(ItemObj item,bool dragging = false) {
			if (dragging) {
				item.tr.parent = tr;
				item.tr.position = new Vector3(Level.me.mouseHud.x,Level.me.mouseHud.y,-8);
			} else {
				Vector2 pos;
				switch (item.origin) {
					case ItemOrigin.Inventory:
						pos = inventorySlots[item.position].tr.position;
						item.tr.parent = tr;
						item.tr.position = new Vector3(pos.x,pos.y,-1);
						break;
					case ItemOrigin.Crafting:
						pos = craftingRows[item.position,0].position;
						item.tr.parent = craftingMenu;
						item.tr.position = new Vector3(pos.x,pos.y,-1);
						break;
					case ItemOrigin.Storage:
						pos = storageSlots[item.position].tr.position;
						item.tr.parent = storageMenu;
						item.tr.position = new Vector3(pos.x,pos.y,-1);
						break;
				}
			}
		}

		public bool GetItem(ItemType type,int quantity = 1) {
			var max = ItemObj.MaxStack(type);
			for (int a = 0; a < inventoryCount && quantity > 0; a++) {
				if (inventory[a] == null) {
					int q;
					if (quantity > max) {
						q = max;
						quantity -= max;
					} else {
						q = quantity;
						quantity = 0;
					}
					var i = ItemObj.Create(type,q);
					i.origin = ItemOrigin.Inventory;
					i.position = a;
					inventory[a] = i;
					UpdateItemPosition(i);
				} else if (inventory[a].type == type && inventory[a].quantity < max) {
					if (quantity+inventory[a].quantity > max) {
						quantity -= max-inventory[a].quantity;
						inventory[a].quantity = max;
					} else {
						inventory[a].quantity += quantity;
						quantity = 0;
					}
				}
			}
			return quantity <= 0;
		}

		public void PlayerHit(int damage) {
			health -= damage;
			if (health <= 0) {
				health = 0;
				GameOver();
			}
		}

		public void Consume() {
			if (health <= 0) return;
			switch (selectedItem.type) {
				case ItemType.FilledBucket:
					thirsty += .15f;
					break;
				case ItemType.Carrot:
					hungry += .1f;
					break;
				case ItemType.RawBeef:
					hungry += .2f;
					PlayerHit(2);
					break;
				case ItemType.RoastBeef:
					hungry += .3f;
					PlayerHit(2);
					break;
			}
			if (thirsty > 1) thirsty = 1;
			if (hungry > 1) hungry = 1;
			if (selectedItem.type == ItemType.FilledBucket) {
				selectedItem.type = ItemType.Bucket;
			} else {
				selectedItem.quantity--;
				if (selectedItem.quantity <= 0) {
					Destroy(selectedItem.gameObject);
					selectedItem = null;
				}
			}
		}

		public bool FillBucket() {
			if (selectedItem != null && selectedItem.type == ItemType.Bucket) {
				selectedItem.type = ItemType.FilledBucket;
				return true;
			}
			return false;
		}

		public void DrinkFromRain(float amount) {
			if (thirsty < 1) {
				thirsty += Time.deltaTime*.015f;
				if (thirsty > 1) thirsty = 1;
			}
		}

		public bool InteractWithPlantation(Plantation plantation) {
			if (selectedItem != null) {
				switch (selectedItem.type) {
					case ItemType.Hoe:
					case ItemType.Bucket:
						Level.me.ConsumePlayerClick();
						return true;
					case ItemType.CarrotSeed:
						if (plantation.PlantSeed()) {
							selectedItem.quantity--;
							if (selectedItem.quantity <= 0) {
								Destroy(selectedItem.gameObject);
								selectedItem = null;
							}
						}
						Level.me.ConsumePlayerClick();
						return true;
					case ItemType.FilledBucket:
						bool watered = plantation.Water();
						int p;
						p = plantation.fixedPosition;
						while (true) {
							p++;
							if (p >= Level.fixedLength) p = 0;
							if (p == plantation.fixedPosition) break;
							if (Level.me.fixedEntities[p] == null) break;
							if (Level.me.fixedEntities[p].type != FixedEntityType.Plantation) break;
							if (Level.me.fixedEntities[p].GetComponent<Plantation>().Water()) watered = true;
						}
						p = plantation.fixedPosition;
						while (true) {
							p--;
							if (p < 0) p = Level.fixedLength-1;
							if (p == plantation.fixedPosition) break;
							if (Level.me.fixedEntities[p] == null) break;
							if (Level.me.fixedEntities[p].type != FixedEntityType.Plantation) break;
							if (Level.me.fixedEntities[p].GetComponent<Plantation>().Water()) watered = true;
						}
						if (watered) {
							selectedItem.type = ItemType.Bucket;
							plantation.PlaySfxWater();
						}
						Level.me.ConsumePlayerClick();
						return true;
				}
			}
			return false;
		}

		public bool Consume(ItemType type,int quantity = 1) {
			for (int a = 0; a < inventoryCount; a++) {
				var i = inventory[a];
				if (i != null && i.type == type) {
					var iq = i.quantity;
					if (iq <= quantity) {
						Destroy(i.gameObject);
						inventory[a] = null;
					} else {
						i.quantity -= quantity;
					}
					quantity -= iq;
					if (quantity <= 0) return true;
				}
			}
			return false;
		}

		public void PlaySfx(bool click) {
			aud.PlayOneShot(click ? sfxClick : sfxHover);
		}

		void GameOver() {
			deadMessage.enabled = true;
			deadMessage2.enabled = true;
		}

		Vector2 WorldToHud(Vector2 pos) {
			return cam.ViewportToWorldPoint(Level.me.tripod.cam.WorldToViewportPoint(pos));
		}
	}

	public enum HudHomeMenu {
		None,
		Crafting,
		Storage,
	}

	public struct ItemRecipe {
		public ItemType[] type;
		public int[] quantity;
		public ItemRecipe(ItemType t0,int q0,ItemType t1,int q1,ItemType t2 = ItemType.Axe,int q2 = 0,ItemType t3 = ItemType.Axe,int q3 = 0) {
			type = new ItemType[4];
			quantity = new int[4];
			type[0] = t0;
			quantity[0] = q0;
			type[1] = t1;
			quantity[1] = q1;
			type[2] = t2;
			quantity[2] = q2;
			type[3] = t3;
			quantity[3] = q3;
		}
	}
}