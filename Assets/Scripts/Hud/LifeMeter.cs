﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class LifeMeter:MonoBehaviour {
		public float value = 1;

		Renderer fill;

		void Awake() {
			fill = transform.Find("fill").GetComponent<Renderer>();
		}

		void Start() {
			UpdateValue();
		}

		void Update() {
			UpdateValue();
		}

		void UpdateValue() {
			float colour;
			if (value >= .65f) {
				colour = 0;
			} else if (value >= .35f) {
				colour = 1;
			} else if (value >= .1f) {
				colour = 2;
			} else {
				colour = 3;
			}
			fill.material.mainTextureOffset = new Vector3(Mathf.Lerp(.25f,-63.5f,value),colour*.25f+.0625f);
		}
	}
}