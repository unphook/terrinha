﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class WindowCam:MonoBehaviour {
		public bool fullRect {
			get { return _fullRect; }
			set {
				_fullRect = value;
				if (_fullRect) {
					cam.rect = Window.fullRect;
				} else {
					cam.rect = Window.GetRect(_rect);
				}
			}
		}
		[SerializeField]
		bool _fullRect = true;
		public Rect rect {
			get { return _rect; }
			set {
				_rect = value;
				if (!_fullRect) {
					cam.rect = Window.GetRect(_rect);
				}
			}
		}
		[SerializeField]
		Rect _rect = new Rect(0,0,1,1);

		Camera cam;

		void Awake() {
			cam = GetComponent<Camera>();
		}

		void Start() {
			if (_fullRect) {
				cam.rect = Window.fullRect;
			} else {
				cam.rect = Window.GetRect(_rect);
			}
		}

		void LateUpdate() {
			if (Window.update) {
				Start();
			}
		}
	}
}