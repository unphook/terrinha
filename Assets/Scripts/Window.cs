﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public static class Window {
		public static bool update { get; private set; }
		public static int width { get; private set; }
		public static int height { get; private set; }
		public static float ratio { get; private set; }
		public static float realRatio { get; private set; }
		public static Rect fullRect { get; private set; }

		public const float idealRatio = 16/9f;
		public const float ratioTolerance = .05f;

		static bool ratioInsideBounds = false;
		static bool ratioMode = false; //true: > idealRatio; false: < idealRatio
		static float rectOffset = 0;
		static float rectScale = 0;
		
		public static void Start() {
			SetValues();
			update = false;
		}
		
		public static void Update() {
			if (width != Screen.width || height != Screen.height) {
				SetValues();
				update = true;
			} else {
				update = false;
			}
		}
		
		static void SetValues() {
			width = Screen.width;
			height = Screen.height;
			realRatio = ratio = (float)width/height;
			if (ratio > idealRatio+ratioTolerance) {
				ratio = idealRatio;
				ratioInsideBounds = false;
				ratioMode = true;
				rectScale = idealRatio/realRatio;
				rectOffset = (1-rectScale)*.5f;
				fullRect = new Rect(rectOffset,0,rectScale,1);
			} else if (ratio < idealRatio-ratioTolerance) {
				ratio = idealRatio;
				ratioInsideBounds = false;
				ratioMode = false;
				rectScale = realRatio/idealRatio;
				rectOffset = (1-rectScale)*.5f;
				fullRect = new Rect(0,rectOffset,1,rectScale);
			} else {
				ratioInsideBounds = true;
				fullRect = new Rect(0,0,1,1);
			}
		}
		
		public static Rect GetRect(Rect rect) {
			if (ratioInsideBounds) return rect;
			return GetRectInternal(rect.xMin,rect.yMin,rect.xMax,rect.yMax);
		}
		
		public static Rect GetRect(float x,float y,float width,float height) {
			if (ratioInsideBounds) return new Rect(x,y,width,height);
			return GetRectInternal(x,y,x+width,y+height);
		}
		
		static Rect GetRectInternal(float x0,float y0,float x1,float y1) {
			if (ratioMode) {
				if (x0 <= 0) {
					x0 = rectOffset;
				} else {
					x0 = rectOffset+rectScale*x0;
				}
				if (x1 >= 1) {
					x1 = rectOffset+rectScale;
				} else {
					x1 = rectOffset+rectScale*x1;
				}
			} else {
				if (y0 <= 0) {
					y0 = rectOffset;
				} else {
					y0 = rectOffset+rectScale*y0;
				}
				if (y1 >= 1) {
					y1 = rectOffset+rectScale;
				} else {
					y1 = rectOffset+rectScale*y1;
				}
			}
			return Rect.MinMaxRect(x0,y0,x1,y1);
		}
	}
}